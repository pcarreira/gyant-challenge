const fs = require('fs');
require('dotenv').config();

const { deleteAllConditions, createCondition, deleteAllCases, createCase } = require('./database');

async function loadConditions() {
	const data = await fs.readFileSync('./files/conditions.csv', 'utf8');
	await deleteAllConditions();
	const inserts = data.split('\n')
		.slice(1)
		.map(line => {
			const [code, description] = line.split("\t");
			if(code && description) {
				return createCondition(code, description);
			}
		});

	return Promise.all(inserts);
}

async function loadCases() {
	const files = fs.readdirSync('./files/cases');
	await deleteAllCases();
	const inserts = files.map(async (filename) => {
		const fileText = await fs.readFileSync(`./files/cases/${filename}`, 'utf8');
		return createCase(fileText);
	});
	return Promise.all(inserts);
}

async function load() {
	await loadCases();
	await loadConditions();
	process.exit();
}

load();