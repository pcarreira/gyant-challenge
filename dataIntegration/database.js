const mongoose = require('mongoose');
mongoose.connect(process.env.MONGO_DB, { useNewUrlParser: true, useUnifiedTopology: true });

const Condition = mongoose.model('Condition', {
	code: String,
	description: String
}, 'condition');

const Case = mongoose.model('Case', {
	text: String
}, 'case');

function deleteAllConditions() {
	return Condition.deleteMany({});
}

function createCondition(code, description) {
	return new Condition({ code, description }).save();
}

function deleteAllCases() {
	return Case.deleteMany({});
}

function createCase(text) {
	return new Case({ text }).save();
}

module.exports = {
	deleteAllConditions,
	createCondition,
	deleteAllCases,
	createCase
}