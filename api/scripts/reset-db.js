const mongoose = require('mongoose');
require('dotenv').config();

mongoose.connect(process.env.MONGO_DB, { useNewUrlParser: true, useUnifiedTopology: true });

const RegisterModel = mongoose.model('Register', new mongoose.Schema({}, { strict: false }), 'register');
const UserModel = mongoose.model('User', new mongoose.Schema({}, { strict: false }), 'user');


async function resetDb() {
	await UserModel.deleteMany({ _id: "5e7755609f81320379a924c2" });
	await new UserModel({
		_id: "5e7755609f81320379a924c2",
		name:"Test user",
		username:"test",
		password:"9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08"
	}).save();

	await RegisterModel.deleteMany({ user: mongoose.Types.ObjectId("5e7755609f81320379a924c2") });

	process.exit(0);
}

resetDb();