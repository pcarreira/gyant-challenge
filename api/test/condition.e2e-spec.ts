import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';

describe('ConditionController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
		jest.setTimeout(30000);
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('POST /condition should return 200 for valid user', () => {
    return request(app.getHttpServer())
			.get('/condition')
			.set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJJZCI6IjVlNzc1NTYwOWY4MTMyMDM3OWE5MjRjMiJ9LCJpYXQiOjE1ODUwMDUxNTMsImV4cCI6OTk1ODUwMDg3NTN9.qE6MDTk1NqeIjchGX89wRIVsI_li3o9LCeIKGnbAef8')
      .expect(200);
	});
});
