import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';

describe('CaseController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
		jest.setTimeout(30000);
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('GET /case should return 200 with the first case', () => {
    return request(app.getHttpServer())
			.get('/case')
			.set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJJZCI6IjVlNzc1NTYwOWY4MTMyMDM3OWE5MjRjMiJ9LCJpYXQiOjE1ODUwMDUxNTMsImV4cCI6OTk1ODUwMDg3NTN9.qE6MDTk1NqeIjchGX89wRIVsI_li3o9LCeIKGnbAef8')
			.expect(200)
			.expect({
				id: '5e77ebd330c7db22e1ac4d27',
				text: 'Patient  is an 45 year old  female.    Chief Complaint:  Problem    HPI  states that about one month ago she woke up with redness and swelling to her left eye.  She went to see an ophthalmologist who prescribed her naphazoline.  She states that this relieves the redness only temporarily.  She also states that this morning she awoke with more crusting to the left eye.  The eye is not particularly itchy, but seems more irritated today.  She has not had any sick contacts.          Review of Systems   Constitutional: Negative for fever.   Eyes: Positive for discharge and redness. Negative for blurred vision, double vision and photophobia.   Skin: Negative for itching.   Neurological: Positive for headaches.         Objective:     BP 100/69  -Strict ER precautions reviewed with patient should symptoms persist or worsen (specific signs reviewed verbally).  Good communication established and plan agreed upon by patient.\n'
			});
	});
});
