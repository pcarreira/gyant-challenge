import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';

describe('AuthController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
		jest.setTimeout(30000);
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('POST /auth/login should return 200 for valid user', () => {
    return request(app.getHttpServer())
			.post('/auth/login')
			.send({
				username: "test",
				password: "test"
			})
      .expect(200);
	});
	
	it('POST /auth/login should return 401 for an invalid username', () => {
    return request(app.getHttpServer())
			.post('/auth/login')
			.send({
				username: "invalid",
				password: "test"
			})
      .expect(401)
      .expect({
				statusCode: 401, 
				message: 'Invalid username or password'
			});
	});
	
	it('POST /auth/login should return 401 for an invalid password', () => {
    return request(app.getHttpServer())
			.post('/auth/login')
			.send({
				username: "test",
				password: "invalid"
			})
      .expect(401)
      .expect({
				statusCode: 401, 
				message: 'Invalid username or password'
			});
	});
	
	it('POST /auth/login should return 401 for invalid username and password', () => {
    return request(app.getHttpServer())
			.post('/auth/login')
			.send({
				username: "invalid",
				password: "invalid"
			})
      .expect(401)
      .expect({
				statusCode: 401, 
				message: 'Invalid username or password'
			});
  });
});
