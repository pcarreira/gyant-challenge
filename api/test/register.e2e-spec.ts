import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';
import { RegisterSchema } from '../src/schema/register.model';
import { MongooseModule } from '@nestjs/mongoose';

describe('RegisterController (e2e)', () => {
	let app: INestApplication;

	beforeEach(async () => {
		jest.setTimeout(30000);
		const moduleFixture: TestingModule = await Test.createTestingModule({
			imports: [AppModule],
		}).compile();

		app = moduleFixture.createNestApplication();
		await app.init();
	});

	it('POST /register should return 200 for one valid register', () => {
		return request(app.getHttpServer())
			.post('/register')
			.set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJJZCI6IjVlNzc1NTYwOWY4MTMyMDM3OWE5MjRjMiJ9LCJpYXQiOjE1ODUwMDUxNTMsImV4cCI6OTk1ODUwMDg3NTN9.qE6MDTk1NqeIjchGX89wRIVsI_li3o9LCeIKGnbAef8')
			.send({
				case: '5e77ebd330c7db22e1ac4d27',
				condition: '5e77ebd430c7db22e1ac4d2a',
				timeToDecide: 1000
			})
			.expect(201);
	});

	it('POST /register should return 400 for with duplicated code', () => {
		return request(app.getHttpServer())
			.post('/register')
			.set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJJZCI6IjVlNzc1NTYwOWY4MTMyMDM3OWE5MjRjMiJ9LCJpYXQiOjE1ODUwMDUxNTMsImV4cCI6OTk1ODUwMDg3NTN9.qE6MDTk1NqeIjchGX89wRIVsI_li3o9LCeIKGnbAef8')
			.send({
				case: '5e77ebd330c7db22e1ac4d27',
				condition: '5e77ebd430c7db22e1ac4d2a',
				timeToDecide: 1000
			})
			.expect(400)
			.expect({
				statusCode: 400, 
				message: 'User already registered this case id'
			});
	});

	it('POST /register should return 400 for invalid case id', () => {
		return request(app.getHttpServer())
			.post('/register')
			.set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJJZCI6IjVlNzc1NTYwOWY4MTMyMDM3OWE5MjRjMiJ9LCJpYXQiOjE1ODUwMDUxNTMsImV4cCI6OTk1ODUwMDg3NTN9.qE6MDTk1NqeIjchGX89wRIVsI_li3o9LCeIKGnbAef8')
			.send({
				case: '5e77ebd330c7db22e1ac4d35',
				condition: '5e77ebd430c7db22e1ac4d2a',
				timeToDecide: 1000
			})
			.expect(400)
			.expect({
				statusCode: 400, 
				message: 'Case id not found'
			});
	});

	it('POST /register should return 400 for invalid condition id', () => {
		return request(app.getHttpServer())
			.post('/register')
			.set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJJZCI6IjVlNzc1NTYwOWY4MTMyMDM3OWE5MjRjMiJ9LCJpYXQiOjE1ODUwMDUxNTMsImV4cCI6OTk1ODUwMDg3NTN9.qE6MDTk1NqeIjchGX89wRIVsI_li3o9LCeIKGnbAef8')
			.send({
				case: '5e77ebd330c7db22e1ac4d27',
				condition: '5e77ebd430c7db22e1ac4d1a',
				timeToDecide: 1000
			})
			.expect(400)
			.expect({
				statusCode: 400, 
				message: 'Condition id not found'
			});
	});
});
