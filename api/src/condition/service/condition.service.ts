import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { Condition } from '../model/condition.model';

@Injectable()
export class ConditionService {
	constructor(
		@InjectModel('Condition') private readonly conditionModel: Model<Condition>,
	) {
	}

	async list() {
		const result = await this.conditionModel.find();
		return result;
	}

	async exists(conditionId) {
		const registerData = await this.conditionModel.findOne({ _id: conditionId });
		return registerData ? true : false;
	}
}
