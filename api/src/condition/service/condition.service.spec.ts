import { Test, TestingModule } from '@nestjs/testing';
import { ConditionService } from './condition.service';
import { getModelToken } from '@nestjs/mongoose';

describe('AuthService', () => {
	let service: ConditionService;

	beforeEach(async () => {
		this.conditions = [
			{
				_id: "5e77ebd430c7db22e1ac4d2b",
				code: "A64",
				description: "Unspecified sexually transmitted disease",
				__v: 0
			},
			{
				_id: "5e77ebd430c7db22e1ac4d2a",
				code: "A09",
				description: "Infectious gastroenteritis and colitis, unspecified",
				__v: 0
			},
			{
				_id: "5e77ebd430c7db22e1ac4d2c",
				code: "B300",
				description: "Keratoconjunctivitis due to adenovirus",
				__v: 0
			}
		];

		function mockDatabase() {
			this.find = () => {
				return Promise.resolve([
					{
						_id: "5e77ebd430c7db22e1ac4d2b",
						code: "A64",
						description: "Unspecified sexually transmitted disease",
						__v: 0
					},
					{
						_id: "5e77ebd430c7db22e1ac4d2a",
						code: "A09",
						description: "Infectious gastroenteritis and colitis, unspecified",
						__v: 0
					},
					{
						_id: "5e77ebd430c7db22e1ac4d2c",
						code: "B300",
						description: "Keratoconjunctivitis due to adenovirus",
						__v: 0
					}
				]);
			};

			this.findOne = (query) => {
				if(query._id === '5e77ebd330c7db22e1ac4d28'){
					return Promise.resolve(null);
				}
				return Promise.resolve({});
			};
		}

		const module: TestingModule = await Test.createTestingModule({
			providers: [
				ConditionService,
				{
					provide: getModelToken('Condition'),
					useValue: new mockDatabase(),
				}
			],
		}).compile();

		service = module.get<ConditionService>(ConditionService);
	});

	it('should be defined', () => {
		expect(service).toBeDefined();
	});

	describe('List', () => {
		it('should return the conditions list', async () => {
			const result = await service.list();
			expect(result).toEqual(this.conditions);
		});
	});

	describe('Exists', () => {
		it('should return true if the condition id already exists', async () => {
			const result = await service.exists('5e77ebd330c7db22e1ac4d27');
			expect(result).toEqual(true);
		});

		it('should return false if the condition id not exists', async () => {
			const result = await service.exists('5e77ebd330c7db22e1ac4d28');
			expect(result).toEqual(false);
		});
	});
});
