import { Controller, HttpException, HttpStatus, HttpCode, Get, UseGuards } from '@nestjs/common';
import { ConditionService } from './service/condition.service';
import { JwtGuard } from '../guard/jwtGuard';

@Controller('condition')
export class ConditionController {
  constructor(private service: ConditionService) {
  }

	@Get('')
	@UseGuards(new JwtGuard())
	@HttpCode(200)
  async list() {
		const conditions = await this.service.list();
		if(!conditions || conditions.length === 0) {
			throw new HttpException('No conditions available', HttpStatus.NO_CONTENT);
		}
		return conditions.map(condition => ({
			id: condition._id,
			code: condition.code,
			description: condition.description
		}));
  }
}