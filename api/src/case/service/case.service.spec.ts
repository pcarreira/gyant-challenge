import { Test, TestingModule } from '@nestjs/testing';
import { CaseService } from './case.service';
import { getModelToken } from '@nestjs/mongoose';
import { userInfo } from 'os';

describe('AuthService', () => {
	let service: CaseService;

	beforeEach(async () => {
		function mockRegisterDatabase() {
			this.find = (query) => {
				if (query.user === 'userid1') {
					return Promise.resolve([]);
				}
				if (query.user === 'userid2') {
					return Promise.resolve([
						{
							_id: 'xxx',
							user: 'userid2',
							case: '5e77ebd330c7db22e1ac4d27'
						}
					]);
				}
				if (query.user === 'userid3') {
					return Promise.resolve([
						{
							_id: 'registerid1',
							user: 'userid2',
							case: '5e77ebd330c7db22e1ac4d27'
						},
						{
							_id: 'registerid2',
							user: 'userid2',
							case: '5e77ebd330c7db22e1ac4d28'
						}
					]);
				}
				return Promise.resolve([]);
			};
		}

		function mockCaseDatabase() {
			this.findOne = (query) => {
				if (query._id === '5e77ebd330c7db22e1ac4d28') {
					return Promise.resolve(null);
				}
				if (query._id === '5e77ebd330c7db22e1ac4d27') {
					return Promise.resolve({});
				}

				if (query._id['$nin'].length === 0) {
					return Promise.resolve({
						_id: 'idcase1',
						text: 'Case text'
					});
				}
				if (query._id['$nin'].includes('5e77ebd330c7db22e1ac4d27') && query._id['$nin'].includes('5e77ebd330c7db22e1ac4d28')) {
					return Promise.resolve(null);
				}
				if (query._id['$nin'].includes('5e77ebd330c7db22e1ac4d27')) {
					return Promise.resolve({
						_id: 'idcase2',
						text: 'Case text 2'
					});
				}
				return Promise.resolve(null);
			};
		}

		const module: TestingModule = await Test.createTestingModule({
			providers: [
				CaseService,
				{
					provide: getModelToken('Case'),
					useValue: new mockCaseDatabase(),
				},
				{
					provide: getModelToken('Register'),
					useValue: new mockRegisterDatabase(),
				}
			],
		}).compile();

		service = module.get<CaseService>(CaseService);
	});

	it('should be defined', () => {
		expect(service).toBeDefined();
	});

	describe('FindNextCase', () => {
		it('should return the first case for one user without registers', async () => {
			const result = await service.findNextCase('userid1');
			expect(result).toEqual({
				_id: "idcase1",
				text: "Case text"
			});
		});

		it('should return the second case for one user with the first case registered', async () => {
			const result = await service.findNextCase('userid2');
			expect(result).toEqual({
				_id: "idcase2",
				text: "Case text 2"
			});
		});

		it('should return the second case for one user with the first case registered', async () => {
			const result = await service.findNextCase('userid3');
			expect(result).toEqual(null);
		});
	});

	describe('Exists', () => {
		it('should return true if the case already exists', async () => {
			const result = await service.exists('5e77ebd330c7db22e1ac4d27');
			expect(result).toEqual(true);
		});

		it('should return false if the case not exists', async () => {
			const result = await service.exists('5e77ebd330c7db22e1ac4d28');
			expect(result).toEqual(false);
		});
	});
});
