import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { Case } from '../model/case.model';
import { Register } from 'src/register/model/register.model';

@Injectable()
export class CaseService {
	constructor(
		@InjectModel('Case') private readonly caseModel: Model<Case>,
		@InjectModel('Register') private readonly registerModel: Model<Register>,
	) {
	}

	async findNextCase(userId) {
		const userRegisterList = await this.registerModel.find({ user: userId });
		const registeredCases = (userRegisterList || []).map(register => register.case);
		return this.caseModel.findOne({ _id: { $nin: registeredCases }});
	}

	async exists(caseId) {
		const caseData = await this.caseModel.findOne({ _id: caseId });
		return caseData ? true : false;
	}
}
