import { Controller, HttpException, HttpStatus, HttpCode, Get, UseGuards, Headers } from '@nestjs/common';
import { CaseService } from './service/case.service';
import { JwtGuard } from '../guard/jwtGuard';

@Controller('case')
export class CaseController {
  constructor(private service: CaseService) {
  }

	@Get('')
	@UseGuards(JwtGuard)
	@HttpCode(200)
  async list(@Headers('x-user-id') userId: string) {
		const nextCase = await this.service.findNextCase(userId);
		if(!nextCase) {
			throw new HttpException('No cases available', HttpStatus.NO_CONTENT);
		}
		return {
			id: nextCase._id,
			text: nextCase.text
		};
  }
}