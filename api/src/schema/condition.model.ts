import * as mongoose from 'mongoose';

export const ConditionSchema = new mongoose.Schema({
	code: {
		type: String,
		required: true,
		unique: true
	},
	description: {
		type: String,
		required: true
	}
});
