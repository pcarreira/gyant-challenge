import * as mongoose from 'mongoose';

export const CaseSchema = new mongoose.Schema({
	text: {
		type: String,
		required: true
	}
});
