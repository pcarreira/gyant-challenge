import * as mongoose from 'mongoose';

export const RegisterSchema = new mongoose.Schema({
	case: {
		type: 'ObjectId',
		required: true,
		ref: 'case'
	},
	user: {
		type: 'ObjectId',
		required: true,
		ref: 'user'
	},
	timeToDecide: {
		type: Number,
		required: true
	},
	creationDate: {
		type: Date,
		required: true
	}
});

RegisterSchema.index({ 'user': 1, 'case': 1 }, { unique: true });