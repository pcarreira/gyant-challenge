import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ConfigModule } from '@nestjs/config';
import { UserSchema } from './schema/user.model';
import { AuthController } from './auth/auth.controller';
import { AuthService } from './auth/service/auth.service';
import { ConditionController } from './condition/condition.controller';
import { ConditionService } from './condition/service/condition.service';
import { ConditionSchema } from './schema/condition.model';
import { CaseController } from './case/case.controller';
import { CaseService } from './case/service/case.service';
import { CaseSchema } from './schema/case.model';
import { RegisterSchema } from './schema/register.model';
import { JwtGuard } from './guard/jwtGuard';
import { RegisterController } from './register/register.controller';
import { RegisterService } from './register/service/register.service';

@Module({
  imports: [
		ConfigModule.forRoot(),
		MongooseModule.forRoot(process.env.MONGO_DB, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true }),
		MongooseModule.forFeature([{
			name: 'User', schema: UserSchema, collection: 'user'
		}]),
		MongooseModule.forFeature([{
			name: 'Condition', schema: ConditionSchema, collection: 'condition'
		}]),
		MongooseModule.forFeature([{
			name: 'Case', schema: CaseSchema, collection: 'case'
		}]),
		MongooseModule.forFeature([{
			name: 'Register', schema: RegisterSchema, collection: 'register'
		}]),
	],
  controllers: [AuthController, ConditionController, CaseController, RegisterController],
  providers: [AuthService, ConditionService, CaseService, RegisterService],
})
export class AppModule {}
