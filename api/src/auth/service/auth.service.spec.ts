import { Test, TestingModule } from '@nestjs/testing';
import { AuthService } from './auth.service';
import { getModelToken } from '@nestjs/mongoose';

describe('AuthService', () => {
	let service: AuthService;

	beforeEach(async () => {
		function mockDatabase() {
			this.findOne = (data) => {
				if (data.username === 'test' && data.password === '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08') {
					return Promise.resolve({
						name: "Test User",
						username: "test",
						password: "9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08"
					});
				}
				return Promise.resolve(null);
			};
		}

		const module: TestingModule = await Test.createTestingModule({
			providers: [
				AuthService,
				{
					provide: getModelToken('User'),
					useValue: new mockDatabase(),
				}
			],
		}).compile();

		service = module.get<AuthService>(AuthService);
	});

	it('should be defined', () => {
		expect(service).toBeDefined();
	});

	describe('Login', () => {
		it('should return true if the user credententials are valid', async () => {
			const result = await service.login({ username: 'test', password: 'test' });
			expect(result).toEqual({
				name: "Test User",
				password: "9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08",
				username: "test"
			});
		});

		it('should return null if the user username are not valid', async () => {
			const result = await service.login({ username: 'invalid', password: 'test' });
			expect(result).toEqual(null);
		});

		it('should return null if the user password are not valid', async () => {
			const result = await service.login({ username: 'test', password: 'invalid' });
			expect(result).toEqual(null);
		});

		it('should return null if the user username and password are not valid', async () => {
			const result = await service.login({ username: 'invalid', password: 'invalid' });
			expect(result).toEqual(null);
		});
	});

	describe('CreateToken', () => {
		it('should return an JWT token', async () => {
			const result = await service.createToken("username", "secret", "1h");
			expect(typeof result).toEqual("string");
		});
	});
});
