import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { sign } from 'jsonwebtoken';
import * as sha from 'sha256';

import { User } from '../model/user.model';
import { AuthLogin } from '../model/auth.model';

@Injectable()
export class AuthService {
	constructor(
		@InjectModel('User') private readonly userModel: Model<User>,
	) {
	}

	async login(credentials: AuthLogin) {
		const result = await this.userModel.findOne({
			username: credentials.username,
			password: sha(credentials.password)
		});
		return result;
	}

	createToken(userId: string, secret: string, expiresIn: string) {
		const userData = {
			userId
		};
		return sign({ data: userData }, secret, { expiresIn });
	}
}
