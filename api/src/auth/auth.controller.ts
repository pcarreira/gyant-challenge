import { Body, Controller, Post, HttpException, HttpStatus, HttpCode } from '@nestjs/common';
import { AuthService } from './service/auth.service';
import { AuthLogin } from './model/auth.model';

@Controller('auth')
export class AuthController {
  constructor(private service: AuthService) {
  }

	@Post('login')
	@HttpCode(200)
  async create(@Body() credentials: AuthLogin) {
		const userData = await this.service.login(credentials);
		if(!userData) {
			throw new HttpException('Invalid username or password', HttpStatus.UNAUTHORIZED);
		}

		const token = this.service.createToken(userData.id, process.env.JWT_SECRET, process.env.JWT_EXPIRATION_TIME);
		return { 
			name: userData.name,
			token 
		};
  }
}