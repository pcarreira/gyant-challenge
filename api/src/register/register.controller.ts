import { Controller, HttpException, HttpStatus, HttpCode, UseGuards, Post, Headers, Body } from '@nestjs/common';
import { RegisterService } from './service/register.service';
import { JwtGuard } from '../guard/jwtGuard';
import { RegisterInput } from './model/register.model';
import { CaseService } from '../case/service/case.service';
import { ConditionService } from '../condition/service/condition.service';

@Controller('register')
export class RegisterController {
	constructor(
		private registerService: RegisterService,
		private caseService: CaseService,
		private conditionService: ConditionService
	) {
	}

	@Post('')
	@UseGuards(new JwtGuard())
	@HttpCode(201)
	async create(@Body() register: RegisterInput, @Headers('x-user-id') userId: string) {
		const caseExists = await this.caseService.exists(register.case);
		if (!caseExists) {
			throw new HttpException('Case id not found', HttpStatus.BAD_REQUEST);
		}

		const conditionExists = await this.conditionService.exists(register.condition);
		if (!conditionExists) {
			throw new HttpException('Condition id not found', HttpStatus.BAD_REQUEST);
		}

		const caseRegistered = await this.registerService.exists(userId, register.case);
		if (caseRegistered) {
			throw new HttpException('User already registered this case id', HttpStatus.BAD_REQUEST);
		}

		const registerSaved = await this.registerService.create(userId, register.case, register.timeToDecide);
		if (!registerSaved) {
			throw new HttpException('Duplicated register', HttpStatus.BAD_REQUEST);
		}
		return;
	}
}