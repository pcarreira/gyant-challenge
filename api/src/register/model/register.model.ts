export interface Register {
	id: string;
	user: string;
	case: string;
	timeToDecide: string;
	creationDate: string;
}

export interface RegisterInput {
	case: string;
	condition: string;
	timeToDecide: number;
}