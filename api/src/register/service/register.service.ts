import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { Register } from '../model/register.model';

@Injectable()
export class RegisterService {
	constructor(
		@InjectModel('Register') private readonly registerModel: Model<Register>,
	) {
	}

	async create(userId: string, caseId: string, timeToDecide: number) {
		try {
			await new this.registerModel({
				user: userId,
				case: caseId,
				timeToDecide,
				creationDate: new Date()
			}).save();
			return true;
		} catch (err) {
			return false;
		}
	}

	async exists(userId, caseId) {
		const registerData = await this.registerModel.findOne({ user: userId, case: caseId });
		return registerData ? true : false;
	}

	async deleteOne(userId, caseId) {
		return await this.registerModel.deleteOne({ user: userId, case: caseId });
	}
}
