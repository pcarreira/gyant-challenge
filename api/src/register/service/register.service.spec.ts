import { Test, TestingModule } from '@nestjs/testing';
import { RegisterService } from './register.service';
import { getModelToken } from '@nestjs/mongoose';

describe('AuthService', () => {
	let service: RegisterService;
	let serviceFind: RegisterService;

	beforeEach(async () => {
		function mockDatabase(data) {
			this.data=data;
			this.save = () => {
				if(this.data.case === '5e77ebd330c7db22e1ac4d28'){
					return Promise.reject('Some error');
				}
				return Promise.resolve(this.data);
			};

			this.findOne = (query) => {
				if(query.case === '5e77ebd330c7db22e1ac4d28'){
					return Promise.resolve(null);
				}
				return Promise.resolve({});
			};

			this.deleteOne = () => {
				return Promise.resolve({ deleted: 1 });
			}
		}

		const module: TestingModule = await Test.createTestingModule({
			providers: [
				RegisterService,
				{
					provide: getModelToken('Register'),
					useValue: mockDatabase,
				}
			],
		}).compile();

		service = module.get<RegisterService>(RegisterService);

		const moduleFind: TestingModule = await Test.createTestingModule({
			providers: [
				RegisterService,
				{
					provide: getModelToken('Register'),
					useValue: new mockDatabase({}),
				}
			],
		}).compile();

		service = module.get<RegisterService>(RegisterService);
		serviceFind = moduleFind.get<RegisterService>(RegisterService);
	});

	it('should be defined', () => {
		expect(service).toBeDefined();
	});

	describe('Create', () => {
		it('should return true and save the new register entry', async () => {
			const result = await service.create('5e7755609f81320379a924c2', '5e77ebd330c7db22e1ac4d27', 1000);
			expect(result).toEqual(true);
		});

		it('should return false when database fails to create new register entry', async () => {
			const result = await service.create('5e7755609f81320379a924c2', '5e77ebd330c7db22e1ac4d28', 1000);
			expect(result).toEqual(false);
		});
	});

	describe('Exists', () => {
		it('should return true if the pair user and case already exists', async () => {
			const result = await serviceFind.exists('5e7755609f81320379a924c2', '5e77ebd330c7db22e1ac4d27');
			expect(result).toEqual(true);
		});

		it('should return false if the pair user and case not exists', async () => {
			const result = await serviceFind.exists('5e7755609f81320379a924c2', '5e77ebd330c7db22e1ac4d28');
			expect(result).toEqual(false);
		});
	});

	describe('DeleteOne', () => {
		it('should return delete object', async () => {
			const result = await serviceFind.deleteOne('5e7755609f81320379a924c2', '5e77ebd330c7db22e1ac4d27');
			expect(result).toEqual({ deleted: 1 });
		});
	});
});
