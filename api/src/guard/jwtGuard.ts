import { Injectable, CanActivate, ExecutionContext, HttpStatus, HttpException } from '@nestjs/common';
import { Observable } from 'rxjs';
import { verify } from 'jsonwebtoken';

@Injectable()
export class JwtGuard implements CanActivate {
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
		const request = context.switchToHttp().getRequest();
		const { authorization } = request.headers;
		try {
			const { data: { userId }} = verify(authorization.replace('Bearer ', ''), process.env.JWT_SECRET);
			request.headers = {
				...request.headers,
				'x-user-id': userId
			};
			return true;
		}catch (err) {
			throw new HttpException('Invalid authorization', HttpStatus.UNAUTHORIZED);
		}
  }
}