let currentCase;
let startCaseDate;

$('#selectReasonForm').submit(registerCase);
$('#caseSuccess').hide();
$('#caseDone').hide();

function onLoad() {
	const authToken = sessionStorage.getItem("auth_token");
	if (!authToken) {
		window.location.href = "./index.html";
	}

	const userName = sessionStorage.getItem("user_name");
	$("#userName").text(userName);
	getNextCase();
	getReasons();
}

function logout() {
	sessionStorage.removeItem("auth_token");
	sessionStorage.removeItem("user_name");
	window.location.href = "./index.html";
}

function getNextCase() {
	$.ajax({
		url: `${window.config.server}/case`,
		type: "GET",
		dataType: 'json',
		contentType: 'application/json',
		headers: {
			Authorization: `Bearer ${sessionStorage.getItem("auth_token")}`
		},
		success: caseSuccess,
		error: caseError
	});
}

function caseSuccess(response) {
	$('#caseDone').hide();
	$('#reasonSelect').attr('readonly', false);
	$('#nextCase').show();
	if (response && response.text) {
		currentCase = response;
		$("#caseTextarea").text(response.text);
		startCaseDate = new Date();
	} else {
		$('#caseDone').show();
		$('#reasonSelect').attr('readonly', true);
		$('#nextCase').hide();
	}
}

function caseError(response) {
	console.log(response);
}

function getReasons() {
	$.ajax({
		url: `${window.config.server}/condition`,
		type: "GET",
		dataType: 'json',
		contentType: 'application/json',
		headers: {
			Authorization: `Bearer ${sessionStorage.getItem("auth_token")}`
		},
		success: reasonSuccess,
		error: reasonError
	});
}

function reasonSuccess(response) {
	if (response && response.length > 0) {
		$("#reasonSelect").html();
		response.forEach(reason => {
			$("#reasonSelect").append(`<option value="${reason.id}">${reason.description} (${reason.code})</option>`);
		});
	}
}

function reasonError(error) {
	console.log(error);
}

function registerCase(event) {
	event.preventDefault();
	const difference = new Date() - startCaseDate;
	$.ajax({
		url: `${window.config.server}/register`,
		type: "POST",
		dataType: 'json',
		contentType: 'application/json',
		headers: {
			Authorization: `Bearer ${sessionStorage.getItem("auth_token")}`
		},
		data: JSON.stringify({
			case: currentCase.id,
			condition: $("#reasonSelect").val()[0],
			timeToDecide: difference
		}),
		success: () => {
			$("#caseSuccess").show();
			setTimeout(() => {
				$("#caseSuccess").hide();
			}, 3000);
		},
		error: (response) => {
			if (response.status === 201) {
				$("#caseSuccess").show();
				getNextCase();
				setTimeout(() => {
					$("#caseSuccess").hide();
				}, 3000);
			}
		}
	});
}