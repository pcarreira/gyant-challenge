$('#loginForm').submit(login);

function login(event) {
	event.preventDefault();
	const username = this.elements['username'].value;
	const password = this.elements['password'].value;
	$('.errorMessage').hide();

	$.ajax({
		url: `${window.config.server}/auth/login`,
		type: "POST",
		dataType: 'json',
		contentType: 'application/json',
		data: JSON.stringify({
			username,
			password
		}),
		success: loginSuccess,
		error: loginError
	});
}

function loginSuccess(response) {
	sessionStorage.setItem("auth_token", response.token);
	sessionStorage.setItem("user_name", response.name);
	window.location.href = "./caseLabel.html";
}

function loginError(error) {
	if(error.status === 401) {
		$('#wrongPassword').show();
		return;
	}
	$('#serverError').show();
}